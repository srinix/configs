set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-autoformat/vim-autoformat'
Plugin 'tomasiser/vim-code-dark'
Plugin 'vim-airline/vim-airline'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'preservim/nerdtree'
call vundle#end()            
filetype plugin indent on
syntax on
set relativenumber
let g:formatters_python = ['black']
au BufWrite * :Autoformat
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 0
colorscheme codedark
nnoremap <C-p> :Files<CR>
nnoremap <C-g> :Ag<CR>
nnoremap <C-t> :tab term<CR>
tnoremap <Esc><Esc> <C-W>N
nnoremap <C-S-c> :call system('xclip -sel clip', @0)<CR>
autocmd VimEnter * NERDTree | wincmd p
let g:NERDTreeWinPos = "right"
let g:NERDTreeMinimalUI = 1
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
autocmd BufWinEnter * if getcmdwintype() == '' | silent NERDTreeMirror | endif
